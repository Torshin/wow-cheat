package com.example.wowcheat.services;

import java.util.List;

public interface Solver {
    List<String> findWords(String input);
}
