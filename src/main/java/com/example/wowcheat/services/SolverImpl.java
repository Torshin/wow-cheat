package com.example.wowcheat.services;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SolverImpl implements Solver {
    private final List<String> dict
            = Files.readAllLines(Path.of(new ClassPathResource("wordlist.10000.txt").getFile().getPath()));

    public SolverImpl() throws IOException {
    }

    @Override
    public List<String> findWords(String input) {
        Deque<Character> inputChars = input.chars().mapToObj(i -> ((char) i)).collect(Collectors.toCollection(ArrayDeque<Character>::new));
        return findWords(new ArrayDeque<>(), inputChars);
    }

    private List<String> findWords(Deque<Character> wordDeque, Deque<Character> input) {
        List<String> result = new ArrayList<>();
        List<String> filtered = dict;
        for (Character character : input) {
            ArrayDeque<Character> characters = new ArrayDeque<>(input);
            characters.remove(character);
            wordDeque.addLast(character);
            String word = wordDeque.stream().map(Object::toString).collect(Collectors.joining());
            filtered = filtered.stream().filter(s -> s.startsWith(word)).toList();

            if (!filtered.isEmpty()) {
                if (filtered.stream().anyMatch(s -> s.length() == wordDeque.size())) {
                    result.add(word);
                }
                result.addAll(findWords(wordDeque, characters));
            }
            wordDeque.removeLast();
            filtered = dict;
        }
        return result;
    }
}
