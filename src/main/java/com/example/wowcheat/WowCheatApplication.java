package com.example.wowcheat;

import com.example.wowcheat.services.Solver;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.*;
import java.util.stream.Collectors;

@SpringBootApplication
public class WowCheatApplication implements CommandLineRunner {

    private Solver solver;

    public WowCheatApplication(Solver solver) {
        this.solver = solver;
    }

    public static void main(String[] args) {
        SpringApplication.run(WowCheatApplication.class, args);
    }

    @Override
    public void run(String... args) {
//        Scanner scan = new Scanner(System.in);
//        String line = scan.nextLine();
        List<String> words = solver.findWords("dylai");
        Collections.sort(words, Comparator.comparingInt(String::length));
        System.out.println("======================================");
        System.out.println(String.join("\n ", new LinkedHashSet<>(words)));
    }
}
